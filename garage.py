from gpiozero import DigitalInputDevice, DigitalOutputDevice


class Garage(object):
    def __init__(self):
        self.__door_closed_sensor = DigitalInputDevice(27, pull_up=True)  # orange
        self.__door_closed_sensor.when_activated = lambda: self.__door_closed()
        self.__door_closed_sensor.when_deactivated = lambda: self.__door_open()

        self.__door_toggle_switch = DigitalOutputDevice(23, active_high=False)  # purple

        if self.__door_closed_sensor.value == 1:
            self.__state = 'closed'
        else:
            self.__state = 'open'  # arbitrary

        self.state_changed = None

    def open(self):
        if self.state == 'closed':
            self.__toggle_door_button()

    def close(self):
        if self.state == 'open':
            self.__toggle_door_button()

    def __toggle_door_button(self):
        self.__door_toggle_switch.blink(n=1)

    def __door_open(self):
        self.state = 'open'

    def __door_closed(self):
        self.state = 'closed'

    @property
    def state(self):
        return self.__state

    @state.setter
    def state(self, state):
        if state != self.__state:
            self.__state = state

            if self.state_changed is not None:
                self.state_changed()
