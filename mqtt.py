import logging
import os

import paho.mqtt.client as mqtt

import garage

logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger(__name__)

state_topic = os.environ['STATE_TOPIC']
command_topic = os.environ['COMMAND_TOPIC']
availability_topic = os.environ['AVAIL_TOPIC']


def state(client: mqtt.Client, garage_controller: garage.Garage):
    st = garage_controller.state
    logger.info(st)

    client.publish(state_topic, st, retain=True, qos=1)


def command(client: mqtt.Client, garage_controller: garage.Garage, message: mqtt.MQTTMessage):
    comm = message.payload.decode('utf-8')
    logger.info(comm)

    if comm == 'OPEN':
        garage_controller.open()
    elif comm == 'CLOSE':
        garage_controller.close()


def on_connect(client: mqtt.Client, garage_controller: garage.Garage, flags, reason, properties):
    logger.info(reason)
    logger.info(properties)
    client.publish(availability_topic, 'online', retain=True, qos=1)
    client.subscribe(command_topic)


def main():
    broker_address = os.environ['BROKER']
    garage_controller = garage.Garage()

    client = mqtt.Client(client_id='home/garage/door', userdata=garage_controller, protocol=mqtt.MQTTv5)
    client.enable_logger()

    client.will_set(availability_topic, 'offline', retain=True, qos=1)
    client.on_connect = on_connect
    client.on_message = command

    client.connect(broker_address)

    state(client, garage_controller)

    garage_controller.state_changed = lambda: state(client, garage_controller)

    client.loop_forever(retry_first_connection=True)


if __name__ == '__main__':
    main()
